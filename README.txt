requirements:

python 2.7
python 3.9
python3-venv


download and extract this:

http://ants.aichallenge.org/tools.tar.bz2


Clone this repo to tools/ dir and:

cd g_bot
python3 -m venv venv


To play with example bots, modify play_one_game.sh (in tools dir) to this:

#!/usr/bin/env sh
./playgame.py --player_seed 42 --end_wait=0.25 --verbose --log_dir game_logs --turns 1000 --map_file maps/maze/maze_04p_01.map "$@" "bash g_bot/start_script.sh" "python sample_bots/python/LeftyBot.py" "python sample_bots/python/HunterBot.py" "python sample_bots/python/GreedyBot.py"


Then you can run:

bash play_one_game.sh
